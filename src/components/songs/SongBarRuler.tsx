import React from 'react';

import { Box, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

interface SongBarRulerProps {
    xCoord : number,
    thumbClick : any
}

const useStyles = makeStyles((theme: Theme) => ({
    ruleLine : (positionProp: SongBarRulerProps) => ({
        height: theme.spacing(12),
        width: '0px',
        backgroundColor: 'none',
        borderRight: "dashed thin lightgray",
        left: positionProp.xCoord
    }),
    root: {
      transform: 'translateZ(0px)',
      flexGrow: 1,
      textAlign: 'center'
    },
    wrapper: {
      height: 0,
      width: 0
    },
    ruleDot: {
        height: 10,
        width: 10,
        position: 'relative',
        left: -5,
        bottom: -5,
        backgroundColor: 'lightgray',
        borderRadius : "20px"
    },
  }));

const SongBarRuler: React.FC<SongBarRulerProps>
= (props) => { 
  const classes = useStyles(props);

  return (
    <Box className={classes.wrapper}>   
        { (props.xCoord > 0) ? 
            <Box style={{"position":"absolute"}} className={classes.ruleLine} >
                <Box className={classes.ruleDot} onClick={props.thumbClick} />
            </Box> : ''
        }
    </Box>
  );
}
export default SongBarRuler;
